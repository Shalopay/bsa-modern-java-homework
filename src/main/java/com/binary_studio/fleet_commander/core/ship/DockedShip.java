package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.Subsystem;

public final class DockedShip extends AbstractShip implements ModularVessel {

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powerGridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		super(name, shieldHP, hullHP, powerGridOutput, capacitorAmount, capacitorRechargeRate, speed, size);
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powerGridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powerGridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		fitSubsystemChecker(subsystem);
		setAttackSubsystem(subsystem);
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		fitSubsystemChecker(subsystem);
		setDefenciveSubsystem(subsystem);
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		var currentAttackSubsystem = getAttackSubsystem();
		var currentDefenciveSubsystem = getDefenciveSubsystem();
		if (currentAttackSubsystem == null && currentDefenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		if (currentAttackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		if (currentDefenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		return CombatReadyShip.getCombatReadyShipFromDock(this);
	}

	private void fitSubsystemChecker(Subsystem subsystemToAdd) throws InsufficientPowergridException {
		if (subsystemToAdd == null) {
			return;
		}
		int powerGridInUse = 0;
		var currentAttackSubsystem = getAttackSubsystem();
		var currentDefenciveSubsystem = getDefenciveSubsystem();
		if (subsystemToAdd instanceof AttackSubsystem) {
			powerGridInUse = currentDefenciveSubsystem != null
					? currentDefenciveSubsystem.getPowerGridConsumption().value() : 0;
		}
		if (subsystemToAdd instanceof DefenciveSubsystem) {
			powerGridInUse = currentAttackSubsystem != null ? currentAttackSubsystem.getPowerGridConsumption().value()
					: 0;
		}
		int probableLack = powerGridInUse + subsystemToAdd.getPowerGridConsumption().value()
				- getPowerGridOutput().value();
		if (probableLack > 0) {
			throw new InsufficientPowergridException(probableLack);
		}
	}

}
