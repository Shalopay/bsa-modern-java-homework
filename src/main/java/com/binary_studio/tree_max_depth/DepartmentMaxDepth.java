package com.binary_studio.tree_max_depth;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;
import java.util.stream.Stream;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		return calculateMaxDepthWithoutRecursion(rootDepartment);
	}

	private static Integer calculateMaxDepthInRecursionWay(Department rootDepartment) {
		// verify if rootDepartment is allowed
		if (rootDepartment == null || rootDepartment.name == null) {
			return 0;
		}
		return 1 + rootDepartment.subDepartments.stream()
				// verify if subDepartment is allowed
				.filter(Objects::nonNull)
				// verify if subDepartment has no restricted name
				.filter(department -> department.name != null)
				// calculate maxDepth for current subDepartment via recursion
				.map(DepartmentMaxDepth::calculateMaxDepth)
				// get maximum of subDepartments maxDepths
				.max(Integer::compareTo).orElse(0);
	}

	private static Integer calculateMaxDepthWithoutRecursion(Department rootDepartment) {
		int maxDepth = 0;
		// verify if rootDepartment is allowed
		if (rootDepartment == null || rootDepartment.name == null) {
			return maxDepth;
		}
		// use Queue for BFS (Breadth-first search) of helper class
		Queue<DepartmentWithLevel> queue = new LinkedList<>();
		queue.add(new DepartmentWithLevel(rootDepartment, maxDepth + 1));
		while (!queue.isEmpty()) {
			var departmentWithLevel = queue.poll();
			var department = departmentWithLevel.department;
			var currentLevel = departmentWithLevel.level;
			maxDepth = Math.max(maxDepth, currentLevel);
			// verify if subDepartments isn't null and get stream of it
			Stream.ofNullable(department.subDepartments).flatMap(Collection::stream)
					// verify if subDepartment is allowed
					.filter(Objects::nonNull)
					// verify if subDepartment has no restricted name
					.filter(subDepartment -> subDepartment.name != null)
					// cast subDepartments to helper class type
					.map(subDepartment -> new DepartmentWithLevel(subDepartment, currentLevel + 1))
					// add subDepartments with their level into queue
					.forEach(queue::add);
		}
		return maxDepth;
	}

	// helper class, represents departments with their level in hierarchy
	private static final class DepartmentWithLevel {

		private final Department department;

		private final Integer level;

		private DepartmentWithLevel(Department department, Integer level) {
			this.department = department;
			this.level = level;
		}

	}

}
