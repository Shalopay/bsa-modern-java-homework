package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public abstract class AbstractShip {

	private static final String DEFAULT_SHIP_NAME_MOCK = "Default StarShip - ";

	private static int sideNumber = 1;

	private final String name;

	private final PositiveInteger shieldHP;

	private final PositiveInteger hullHP;

	private final PositiveInteger powerGridOutput;

	private final PositiveInteger capacitorAmount;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger size;

	private final PositiveInteger speed;

	private PositiveInteger currentCapacitorState;

	private PositiveInteger currentShieldHP;

	private PositiveInteger currentHullHP;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	protected AbstractShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powerGridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		// prevent to acquire ships that are named by null or empty name
		if (name == null || name.isBlank()) {
			name = DEFAULT_SHIP_NAME_MOCK + sideNumber++;
		}
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powerGridOutput = powerGridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.currentCapacitorState = capacitorAmount;
		this.currentHullHP = hullHP;
		this.currentShieldHP = shieldHP;
		this.size = speed;
		this.speed = size;
	}

	public String getName() {
		return this.name;
	}

	public PositiveInteger getShieldHP() {
		return this.shieldHP;
	}

	public PositiveInteger getHullHP() {
		return this.hullHP;
	}

	public PositiveInteger getPowerGridOutput() {
		return this.powerGridOutput;
	}

	public PositiveInteger getCapacitorAmount() {
		return this.capacitorAmount;
	}

	public PositiveInteger getCapacitorRechargeRate() {
		return this.capacitorRechargeRate;
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

	protected void setAttackSubsystem(AttackSubsystem attackSubsystem) {
		this.attackSubsystem = attackSubsystem;
	}

	protected void setDefenciveSubsystem(DefenciveSubsystem defenciveSubsystem) {
		this.defenciveSubsystem = defenciveSubsystem;
	}

	protected PositiveInteger getCurrentCapacitorState() {
		return this.currentCapacitorState;
	}

	protected void setCurrentCapacitorState(PositiveInteger currentCapacitorState) {
		this.currentCapacitorState = currentCapacitorState;
	}

	public PositiveInteger getSize() {
		return this.size;
	}

	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	public PositiveInteger getCurrentShieldHP() {
		return this.currentShieldHP;
	}

	public void setCurrentShieldHP(PositiveInteger currentShieldHP) {
		this.currentShieldHP = currentShieldHP;
	}

	public PositiveInteger getCurrentHullHP() {
		return this.currentHullHP;
	}

	public void setCurrentHullHP(PositiveInteger currentHullHP) {
		this.currentHullHP = currentHullHP;
	}

}
