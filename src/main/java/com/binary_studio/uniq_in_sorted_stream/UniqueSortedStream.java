package com.binary_studio.uniq_in_sorted_stream;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;
// imports for 2nd approach
/*import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;*/

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		// #1st approach
		// return Optional.of(stream.distinct()).orElse(Stream.empty());

		// 2nd approach
		/*
		 * if (stream == null) { return Stream.empty(); } Set<Row<T>> set =
		 * Collections.synchronizedSet(new LinkedHashSet<>()); stream.forEach(set::add);
		 * return set.stream();
		 */

		// 3rd approach (to be honestly this was investigated from Internet)
		return Optional.of(stream.filter(distinctByKey(Row::getPrimaryId))).orElse(Stream.empty());
	}

	// utility method for 3rd approach
	public static <T> Predicate<T> distinctByKey(Function<? super T, Object> keyExtract) {
		Map<Object, Boolean> map = new ConcurrentHashMap<>();
		return t -> map.putIfAbsent(keyExtract.apply(t), Boolean.TRUE) == null;
	}

}
