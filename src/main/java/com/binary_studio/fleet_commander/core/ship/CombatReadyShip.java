package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip extends AbstractShip implements CombatReadyVessel {

	private CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powerGridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size, AttackSubsystem attackSubsystem,
			DefenciveSubsystem defenciveSubsystem) {
		super(name, shieldHP, hullHP, powerGridOutput, capacitorAmount, capacitorRechargeRate, speed, size);
		setAttackSubsystem(attackSubsystem);
		setDefenciveSubsystem(defenciveSubsystem);
	}

	public static CombatReadyShip getCombatReadyShipFromDock(DockedShip readyToFlyShip) {
		return new CombatReadyShip(readyToFlyShip.getName(), readyToFlyShip.getShieldHP(), readyToFlyShip.getHullHP(),
				readyToFlyShip.getPowerGridOutput(), readyToFlyShip.getCapacitorAmount(),
				readyToFlyShip.getCapacitorRechargeRate(), readyToFlyShip.getCurrentSpeed(), readyToFlyShip.getSize(),
				readyToFlyShip.getAttackSubsystem(), readyToFlyShip.getDefenciveSubsystem());
	}

	@Override
	public void endTurn() {
		int actualCapacitorValueAfterRecharge = Math.min(
				getCapacitorRechargeRate().value() + getCurrentCapacitorState().value(), getCapacitorAmount().value());
		setCurrentCapacitorState(PositiveInteger.of(actualCapacitorValueAfterRecharge));
	}

	@Override
	public void startTurn() {
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		int probableCapacitorValueAfterAttack = getCurrentCapacitorState().value()
				- getAttackSubsystem().getCapacitorConsumption().value();
		if (probableCapacitorValueAfterAttack < 0) {
			return Optional.empty();
		}
		setCurrentCapacitorState(PositiveInteger.of(probableCapacitorValueAfterAttack));
		return Optional.of(new AttackAction(getAttackSubsystem().attack(target), this, target, getAttackSubsystem()));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		var reducedAttack = getDefenciveSubsystem().reduceDamage(attack);
		int damageRecieved = reducedAttack.damage.value();
		int shieldReduction = Math.min(damageRecieved, getCurrentShieldHP().value());
		setCurrentShieldHP(PositiveInteger.of(getCurrentShieldHP().value() - shieldReduction));
		int damageAfterShieldReduction = damageRecieved - shieldReduction;
		if (damageAfterShieldReduction >= getCurrentHullHP().value()) {
			return new AttackResult.Destroyed();
		}
		int hullDamage = damageAfterShieldReduction > 0
				? Math.min(damageAfterShieldReduction, getCurrentHullHP().value()) : 0;
		setCurrentHullHP(PositiveInteger.of(getCurrentHullHP().value() - hullDamage));

		return new AttackResult.DamageRecived(attack.weapon, PositiveInteger.of(damageRecieved), this);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		int probableCapacitorValueAfterRegen = getCurrentCapacitorState().value()
				- getDefenciveSubsystem().getCapacitorConsumption().value();
		if (probableCapacitorValueAfterRegen < 0) {
			return Optional.empty();
		}
		setCurrentCapacitorState(PositiveInteger.of(probableCapacitorValueAfterRegen));
		var defensiveSubsystemCapability = getDefenciveSubsystem().regenerate();
		int actualShieldRegenValue = Math.min(defensiveSubsystemCapability.shieldHPRegenerated.value(),
				getShieldHP().value() - getCurrentShieldHP().value());
		setCurrentShieldHP(PositiveInteger.of(actualShieldRegenValue + getCurrentShieldHP().value()));
		int actualHullRegenValue = Math.min(defensiveSubsystemCapability.hullHPRegenerated.value(),
				getHullHP().value() - getCurrentHullHP().value());
		setCurrentHullHP(PositiveInteger.of(actualHullRegenValue + getCurrentHullHP().value()));
		return Optional.of(new RegenerateAction(PositiveInteger.of(actualShieldRegenValue),
				PositiveInteger.of(actualHullRegenValue)));
	}

}
