package com.binary_studio.uniq_in_sorted_stream;

import java.util.Objects;

//You CAN modify this class
public final class Row<T> {

	private final T id;

	public Row(T id) {
		this.id = id;
	}

	public T getPrimaryId() {
		return this.id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Row)) {
			return false;
		}
		Row<?> row = (Row<?>) o;
		return Objects.equals(this.id, row.id);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.id);
	}

}
